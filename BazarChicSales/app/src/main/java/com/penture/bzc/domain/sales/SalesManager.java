package com.penture.bzc.domain.sales;

import com.penture.bzc.data.RetrofitConnector;
import com.penture.bzc.domain.entities.Sale;
import com.penture.bzc.domain.sales.actions.GetActiveSales;
import com.penture.bzc.common.eventBus.Messenger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kevin on 07/03/2017.
 */

public class SalesManager {

    private List<Sale> activeSales = new ArrayList<>();
    private boolean isFetchingActiveSales;

    public List<Sale> getActiveSales() {
        return new ArrayList<>(activeSales);
    }

    public void fetchActiveSales() {
        if (isFetchingActiveSales) return;
        isFetchingActiveSales = true;
        (new GetActiveSales(this, RetrofitConnector.getInstance(), Messenger.getInstance()))
                .execute();
    }

    public void onActiveSalesFetched(List<Sale> sales) {
        if (sales != null) activeSales = new ArrayList<>(sales);
        isFetchingActiveSales = false;
    }

    public static SalesManager getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private static class InstanceHolder {
        final static SalesManager INSTANCE = new SalesManager();
    }

}
