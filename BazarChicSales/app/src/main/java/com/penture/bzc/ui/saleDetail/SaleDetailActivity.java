package com.penture.bzc.ui.saleDetail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.ImageView;

import butterknife.BindView;

import com.penture.bzc.R;
import com.penture.bzc.common.utils.StringUtils;
import com.penture.bzc.domain.entities.Illustrations;
import com.penture.bzc.domain.entities.Image;
import com.penture.bzc.domain.entities.Sale;
import com.penture.bzc.domain.sales.SalesManager;
import com.penture.bzc.ui.common.BaseActivity;
import com.squareup.picasso.Picasso;

import static android.text.TextUtils.isEmpty;
import static com.penture.bzc.common.utils.DisplayUtils.getDeviceDensity;
import static com.penture.bzc.ui.sales.SalesAdapter.TAG_SALE_ADAPTER_IMAGE_REQUEST;

/**
 * Created by kevin on 07/03/2017.
 */

public class SaleDetailActivity
        extends BaseActivity {

    private SaleDetailFragment saleDetailFragment;
    private int position;
    private Sale sale;

    @BindView(R.id.toolbar_image) protected ImageView toolbarImage;

    @Override protected int getLayoutId() {
        return R.layout.activity_sale_detail;
    }

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        position = getIntent().getExtras().getInt("position", 0);
        if (position > SalesManager.getInstance().getActiveSales().size()) {
            startSalesActivity();
            return;
        }

        sale = SalesManager.getInstance().getActiveSales().get(position);
        if (sale == null) {
            startSalesActivity();
            return;
        }

        initToolbar(sale.getStore());
        setIllustration(this, sale.getIllustrations(), toolbarImage);

        saleDetailFragment =
                (SaleDetailFragment) getFragmentManager().findFragmentById(R.id.sale_detail);
    }

    @Override protected void onStart() {
        super.onStart();
        saleDetailFragment.displaySale(sale, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initToolbar(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(StringUtils.capitalize(title));
        }
    }

    public static void setIllustration(Context context, Illustrations illustrations,
                                       ImageView imageView) {
        if (illustrations != null) {
            Image mediumImg = illustrations.getLandscapeMedium(getDeviceDensity(context));
            if (imageView != null && !isEmpty(mediumImg.getUrl())) {
                Picasso.with(context)
                        .load(mediumImg.getUrl())
                        .tag(TAG_SALE_ADAPTER_IMAGE_REQUEST)
                        .into(imageView);
                return;
            }
        }
        if (imageView != null) imageView.setImageDrawable(null);
    }
}
