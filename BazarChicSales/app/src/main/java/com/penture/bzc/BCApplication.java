package com.penture.bzc;

import android.app.Application;
import android.util.Log;

import com.penture.bzc.data.RetrofitConnector;

/**
 * Created by kevin on 07/03/2017.
 */

public class BCApplication extends Application {

    @Override public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        RetrofitConnector.init(this);
    }
}
