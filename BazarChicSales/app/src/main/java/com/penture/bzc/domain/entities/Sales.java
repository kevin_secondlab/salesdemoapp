package com.penture.bzc.domain.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kevin on 07/03/2017.
 */

public class Sales {
    @SerializedName("sales") private List<Sale> sales;

    public List<Sale> getSales() {
        return sales;
    }
}
