package com.penture.bzc.common.utils;

import android.content.Context;

/**
 * Created by kevin on 08/03/2017.
 */

public class DisplayUtils {

    public static float getDeviceDensity(Context context) {
        if (context == null) return 0f;
        return context.getResources().getDisplayMetrics().density;
    }
}
