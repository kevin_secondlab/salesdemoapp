package com.penture.bzc.domain.sales.events;

import com.penture.bzc.domain.common.NetworkEvent;

/**
 * Created by kevin on 07/03/2017.
 */

public class SalesFetched extends NetworkEvent {

    public SalesFetched(boolean isSuccess) {
        super(isSuccess);
    }
}
