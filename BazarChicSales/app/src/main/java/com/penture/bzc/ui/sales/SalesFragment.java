package com.penture.bzc.ui.sales;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import butterknife.BindView;

import com.penture.bzc.R;
import com.penture.bzc.domain.entities.Sale;
import com.penture.bzc.ui.common.BaseFragment;

import java.util.List;

/**
 * Created by kevin on 07/03/2017.
 */

public class SalesFragment extends BaseFragment implements SalesAdapter.OnClickArticleListener {

    @BindView(R.id.sales_list) RecyclerView salesList;
    private LinearLayoutManager linearLayoutManager;
    private SalesAdapter salesAdapter;
    private SalesContainer salesContainer;
    public final static String SALES_LIST_STATE_KEY = "recycler_list_state";
    Parcelable salesListState;

    public interface SalesContainer {
        void showSaleDetail(Sale sale, int position);
    }

    @Override protected int getLayoutId() {
        return R.layout.fragment_sales_list;
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        salesContainer = (SalesContainer) getActivity();
        initSalesList();

        if(savedInstanceState != null) {
            salesListState = savedInstanceState.getParcelable(SALES_LIST_STATE_KEY);
        }
    }

    @Override public void onResume() {
        super.onResume();
        if (salesListState != null) {
            linearLayoutManager.onRestoreInstanceState(salesListState);
            salesListState = null;
        }
    }

    @Override public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        salesListState = linearLayoutManager.onSaveInstanceState();
        outState.putParcelable(SALES_LIST_STATE_KEY, salesListState);
    }

    private void initSalesList() {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        salesList.setLayoutManager(linearLayoutManager);
        salesList.setHasFixedSize(true);
        salesList.addItemDecoration(new DividerItemDecoration(getActivity(),
                                                              DividerItemDecoration.VERTICAL));

        salesAdapter = new SalesAdapter(getActivity());
        salesAdapter.setOnClickArticleListener(this);
        salesList.setAdapter(salesAdapter);
    }

    @Override
    public void onClick(Sale sale, int position) {
        salesContainer.showSaleDetail(sale, position);
    }

    public void updateSalesList(List<Sale> sales) {
        if (salesAdapter == null) return;
        salesAdapter.insert(sales, 0);
    }
}
