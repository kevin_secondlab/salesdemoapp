package com.penture.bzc.domain.entities;

import android.support.annotation.FloatRange;
import android.support.annotation.IntDef;
import android.util.DisplayMetrics;

import com.google.gson.annotations.SerializedName;

import java.lang.annotation.Retention;
import java.util.List;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by kevin on 07/03/2017.
 */

public class Illustrations {
    final public static float MDPI = 1f;
    final public static float HDPI = 1.5f;
    final public static float XHDPI = 2f;
    final public static float XXHDPI = 3f;
    final public static float XXXHDPI = 4f;

    @SerializedName("80x85") private Image[] mdpiThumbnails;
    @SerializedName("140x130") private Image[] hdpiThumbnails;
    @SerializedName("185x173") private Image[] xhdpiThumbnails;
    @SerializedName("253x260") private Image[] xxhdpiThumbnails;
    @SerializedName("338x343") private Image[] xxxhdpiThumbnails;

    @SerializedName("385x173") private Image[] mdpiLandscapeMediums;
    @SerializedName("590x213") private Image[] hdpiLandscapeMediums;
    @SerializedName("744x281") private Image[] xhdpiLandscapeMediums;
    @SerializedName("1024x320") private Image[] xxhdpiLandscapeMediums;
    @SerializedName("1536x640") private Image[] xxxhdpiLandscapeMediums;

    public Image getThumbnail(float density) {
        if (density == MDPI) return mdpiThumbnails[0];
        if (density == HDPI) return hdpiThumbnails[0];
        if (density == XXHDPI) return xxhdpiThumbnails[0];
        if (density == XXXHDPI) return xxxhdpiThumbnails[0];

        return xhdpiThumbnails[0];
    }

    public Image getLandscapeMedium(float density) {
        if (density == MDPI) return mdpiLandscapeMediums[0];
        if (density == HDPI) return hdpiLandscapeMediums[0];
        if (density == XXHDPI) return xxhdpiLandscapeMediums[0];
        if (density == XXXHDPI) return xxxhdpiLandscapeMediums[0];

        return xhdpiLandscapeMediums[0];
    }
}
