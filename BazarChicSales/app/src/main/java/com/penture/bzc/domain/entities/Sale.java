package com.penture.bzc.domain.entities;

import android.support.annotation.LayoutRes;

import com.google.gson.annotations.SerializedName;
import com.penture.bzc.R;
import com.penture.bzc.ui.common.ListItemView;

import java.util.Date;

/**
 * Created by kevin on 07/03/2017.
 */

public class Sale implements ListItemView {
    @SerializedName("name") private String name;
    @SerializedName("sale_key") private String saleKey;
    @SerializedName("store") private String store;
    @SerializedName("sale_url") private String saleUrl;
    @SerializedName("image_urls") private Illustrations illustrations;
    @SerializedName("begins") private Date dateBegin;
    @SerializedName("ends") private Date dateEnd;
    @SerializedName("description") private String description;

    //@SerializedName("sale") private String saleDetailUrl;
    //@SerializedName("products") private List<String> productsUrls;
    //@SerializedName("size") private int nbProducts;

    public String getName() {
        return name;
    }

    public String getSaleKey() {
        return saleKey;
    }

    public String getStore() {
        return store;
    }

    public String getSaleUrl() {
        return saleUrl;
    }

    public Illustrations getIllustrations() {
        return illustrations;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public String getDescription() {
        return description;
    }

    public @LayoutRes int getListItemViewType() {
        return R.layout.list_item_sale;
    }
}
