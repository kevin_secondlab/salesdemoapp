package com.penture.bzc.data;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.penture.bzc.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;

/**
 * Created by kevin on 07/03/2017.
 */

public class RetrofitConnector {
    private static final RetrofitConnector instance = new RetrofitConnector();
    private static volatile boolean wasInitialized = false;

    private Retrofit client;

    private RetrofitConnector() {
    }

    public static RetrofitConnector init(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("You must provide a non null context");
        }
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                .create();

        instance.client = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(createOkHttpClient())
                .build();
        wasInitialized = true;
        return instance;
    }

    private static OkHttpClient createOkHttpClient() {
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

        //okHttpBuilder.addInterceptor(new ResponseErrorsInterceptor());
        //if (BuildConfig.DEBUG) {
              HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
              logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
              okHttpBuilder.addInterceptor(logging);
        //}

        okHttpBuilder.addNetworkInterceptor(new HeaderInterceptor());
        return okHttpBuilder.build();
    }

    public static RetrofitConnector getInstance() {
        if (!wasInitialized) {
            throw new IllegalStateException(
                    "You must initialize this class before getting his instance");
        }
        return instance;
    }

    public <T> T create(final Class<T> service) {
        return client.create(service);
    }

    public <T> T getBodyAs(Class<T> type, ResponseBody responseBody) throws IOException {
        if (responseBody == null) return null;
        Converter<ResponseBody, T> converter =
                client.responseBodyConverter(type, new Annotation[0]);
        return converter.convert(responseBody);
    }
}
