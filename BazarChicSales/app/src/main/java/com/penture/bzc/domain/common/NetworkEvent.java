package com.penture.bzc.domain.common;

/**
 * Created by kevin on 07/03/2017.
 */

public class NetworkEvent {
    private boolean isSuccess;

    public NetworkEvent(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public boolean isSuccess() {
        return isSuccess;
    }
}
