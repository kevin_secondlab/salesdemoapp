package com.penture.bzc.data;

import com.penture.bzc.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by kevin on 07/03/2017.
 */

public class HeaderInterceptor implements Interceptor {

    public static final String PARAM_API_KEY = "apiKey";

    @Override public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder requestBuilder = original.newBuilder()
                .header(PARAM_API_KEY, BuildConfig.API_KEY);

        return chain.proceed(requestBuilder.build());
    }
}
