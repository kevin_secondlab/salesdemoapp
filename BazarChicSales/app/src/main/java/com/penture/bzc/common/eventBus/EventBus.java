package com.penture.bzc.common.eventBus;

/**
 * Created by kevin on 07/03/2017.
 */

public interface EventBus {
    void post(Object event);
    void register(Object event);
    void unregister(Object event);
}