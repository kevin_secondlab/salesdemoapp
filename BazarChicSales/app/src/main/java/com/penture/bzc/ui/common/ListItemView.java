package com.penture.bzc.ui.common;

import android.support.annotation.LayoutRes;

/**
 * Created by kevin on 07/03/2017.
 */

public interface ListItemView {
    @LayoutRes int getListItemViewType();
}
