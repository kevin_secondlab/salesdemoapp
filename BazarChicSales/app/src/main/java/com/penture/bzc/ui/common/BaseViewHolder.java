package com.penture.bzc.ui.common;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * Created by kevin on 07/03/2017.
 */

public abstract class BaseViewHolder<T>
        extends RecyclerView.ViewHolder
        implements View.OnClickListener {

    protected T data;

    public BaseViewHolder(ViewGroup parent, @LayoutRes int itemLayoutId) {
        super(LayoutInflater.from(parent.getContext()).inflate(itemLayoutId, parent, false));
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);
    }

    public final void performBind(T item, int position) {
        data = item;
        onBind(item, position);
    }

    protected abstract void onBind(T item, int position);

    @Override public void onClick(View v) {
    }
}
