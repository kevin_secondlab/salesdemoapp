package com.penture.bzc.common.eventBus;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by kevin on 07/03/2017.
 */

public class Messenger {

    public static EventBus getInstance() {
        return InstanceHolder.BUS;
    }

    private Messenger() {}

    private static class InstanceHolder {
        final static MyBus BUS = new MyBus(ThreadEnforcer.MAIN);
    }

    public static class MyBus extends Bus implements EventBus {
        public MyBus() {
        }

        public MyBus(String identifier) {
            super(identifier);
        }

        public MyBus(ThreadEnforcer enforcer) {
            super(enforcer);
        }

        public MyBus(ThreadEnforcer enforcer, String identifier) {
            super(enforcer, identifier);
        }
    }
}
