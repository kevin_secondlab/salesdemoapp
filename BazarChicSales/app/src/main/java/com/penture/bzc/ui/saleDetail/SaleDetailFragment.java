package com.penture.bzc.ui.saleDetail;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;

import com.penture.bzc.R;
import com.penture.bzc.common.utils.StringUtils;
import com.penture.bzc.domain.entities.Illustrations;
import com.penture.bzc.domain.entities.Sale;
import com.penture.bzc.ui.common.BaseFragment;

import java.util.Date;

/**
 * Created by kevin on 07/03/2017.
 */

public class SaleDetailFragment extends BaseFragment {

    @BindView(R.id.image) ImageView image;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.timeLeft) TextView timeLeft;
    @BindView(R.id.description) TextView description;

    private CountDownTimer countDownTimer;
    private long lastRemainingSeconds;

    @Override protected int getLayoutId() {
        return R.layout.fragment_sale_detail;
    }

    @Override public void onResume() {
        super.onResume();
        if (countDownTimer != null) countDownTimer.start();
    }

    @Override public void onPause() {
        super.onPause();
        if (countDownTimer != null) countDownTimer.cancel();
    }

    @Override public void onDestroy() {
        super.onDestroy();
        countDownTimer = null;
    }

    public void displaySale(Sale sale, boolean isIllustrationVisible) {
        name.setText(sale.getName());
        description.setText(sale.getDescription());
        setupIllustration(isIllustrationVisible, sale.getIllustrations());
        setTimeLeft(sale.getDateEnd());
    }

    private void resetTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        timeLeft.setText("");
    }

    private void setTimeLeft(Date dateEnd) {
        resetTimer();

        final String saleEnded = getString(R.string.sale_ended);
        final long end = dateEnd.getTime();

        if (System.currentTimeMillis() >= end) {
            timeLeft.append(saleEnded);
            return;
        }

        long remainingMillis = end - System.currentTimeMillis();
        lastRemainingSeconds = 0;
        countDownTimer = new CountDownTimer(remainingMillis, 100) {
            public void onTick(long ms) {
                if (!isAdded()) {
                    cancel();
                    return;
                }

                long remainingSeconds = (end - System.currentTimeMillis()) / 1000;
                if (remainingSeconds != lastRemainingSeconds) {
                    lastRemainingSeconds = remainingSeconds;
                    String timeLeftString =
                            StringUtils.formatCountdown(getString(R.string.countdown_format), ms);
                    timeLeft.setText(String.format(getString(R.string.sale_end_in), timeLeftString));
                }
            }
            public void onFinish() {
                timeLeft.setText(saleEnded);
            }
        }.start();
    }

    private void setupIllustration(boolean isVisible, Illustrations illustrations) {
        if (!isVisible) {
            image.setVisibility(View.GONE);
        } else {
            image.setVisibility(View.VISIBLE);
            SaleDetailActivity.setIllustration(getActivity(), illustrations, image);
        }
    }
}
