package com.penture.bzc.ui.sales;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;

import com.penture.bzc.R;
import com.penture.bzc.domain.entities.Illustrations;
import com.penture.bzc.domain.entities.Image;
import com.penture.bzc.domain.entities.Sale;
import com.penture.bzc.ui.common.BaseViewHolder;
import com.penture.bzc.ui.common.ListItemView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static android.text.TextUtils.isEmpty;
import static com.penture.bzc.common.utils.DisplayUtils.getDeviceDensity;

/**
 * Created by kevin on 07/03/2017.
 */

public class SalesAdapter extends RecyclerView.Adapter<BaseViewHolder<ListItemView>> {

    public static final String TAG_SALE_ADAPTER_IMAGE_REQUEST = "sale_adapter_image_request";

    private List<Sale> items = new ArrayList<>();
    private Context context;
    private float density;
    private OnClickArticleListener onClickArticleListener;

    public SalesAdapter(Context context) {
        this.context = context;
        this.density = getDeviceDensity(context);
    }

    @Override public BaseViewHolder<ListItemView> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SaleViewHolder(parent, viewType);
    }

    @Override public void onBindViewHolder(BaseViewHolder<ListItemView> holder, int position) {
        holder.performBind(items.get(position), position);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    @Override public int getItemViewType(int position) {
        return items.get(position).getListItemViewType();
    }

    public void insert(List<Sale> sales, int index) {
        if (index == 0 && getItemCount() > 0) items.clear();
        items.addAll(index, sales);
        notifyDataSetChanged();
    }

    public void setOnClickArticleListener(OnClickArticleListener onClickArticleListener) {
        this.onClickArticleListener = onClickArticleListener;
    }

    public interface OnClickArticleListener {
        void onClick(Sale sale, int position);
    }

    public class SaleViewHolder extends BaseViewHolder<ListItemView> {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.thumbnail)
        ImageView thumbnail;

        public SaleViewHolder(ViewGroup parent, @LayoutRes int itemLayoutId) {
            super(parent, itemLayoutId);
        }

        @Override protected void onBind(ListItemView item, int position) {
            Sale sale = (Sale) item;
            name.setText(sale.getName());
            setIllustration(sale);
        }

        private void setIllustration(Sale sale) {
            Illustrations illustrations = sale.getIllustrations();
            if (illustrations != null) {
                Image image = illustrations.getThumbnail(density);
                if (image != null && !isEmpty(image.getUrl())) {
                    Picasso.with(context)
                            .load(image.getUrl())
                            .tag(TAG_SALE_ADAPTER_IMAGE_REQUEST)
                            .into(thumbnail);
                    return;
                }
            }

            Picasso.with(context)
                    .cancelRequest(thumbnail);
            thumbnail.setImageDrawable(null);
        }

        @Override public void onClick(View v) {
            onClickArticleListener.onClick((Sale) data, items.indexOf(data));
        }
    }
}
