package com.penture.bzc.common.utils;

import com.penture.bzc.R;

/**
 * Created by kevin on 08/03/2017.
 */

public class StringUtils {

    public static String capitalize(final String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }

    public static String formatCountdown(String format, long millisecUntilEnd) {
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long diff = millisecUntilEnd;

        long daysLeft = diff / daysInMilli;
        diff = diff % daysInMilli;

        long hoursLeft = diff / hoursInMilli;
        diff = diff % hoursInMilli;

        long minutesLeft = diff / minutesInMilli;
        diff = diff % minutesInMilli;

        long secondsLeft = diff / secondsInMilli;

        if (secondsLeft == 0 && minutesLeft > 0) {
            minutesLeft -= 1;
            secondsLeft = 59;
        }

        if (minutesLeft == 0 && hoursLeft > 0) {
            hoursLeft -= 1;
            minutesLeft = 59;
        }

        if (hoursLeft == 0 && daysLeft > 0) {
            daysLeft -= 1;
            hoursLeft = 23;
        }

        return String.format(format, daysLeft, hoursLeft, minutesLeft, secondsLeft);
    }
}
