package com.penture.bzc.domain.sales.actions;

import android.util.Log;

import com.penture.bzc.domain.sales.SalesManager;
import com.penture.bzc.domain.sales.events.SalesFetched;
import com.penture.bzc.common.eventBus.EventBus;
import com.penture.bzc.data.RetrofitConnector;
import com.penture.bzc.data.sales.SalesApi;
import com.penture.bzc.domain.entities.Sales;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kevin on 07/03/2017.
 */

public class GetActiveSales implements Callback<Sales> {

    private final SalesApi salesApi;
    private final EventBus eventBus;
    private final SalesManager salesManager;

    public GetActiveSales(SalesManager salesManager, RetrofitConnector connector, EventBus eventBus) {
        salesApi = connector.create(SalesApi.class);
        this.eventBus = eventBus;
        this.salesManager = salesManager;
    }

    public void execute() {
        salesApi.getActiveSales().enqueue(this);
    }

    @Override
    public void onResponse(Call<Sales> call, Response<Sales> response) {
        boolean isSuccess = response.isSuccessful() && response.body() != null;
        if (isSuccess) {
            salesManager.onActiveSalesFetched(response.body().getSales());
        } else {
            salesManager.onActiveSalesFetched(null);
        }
        eventBus.post(new SalesFetched(isSuccess));
    }

    @Override
    public void onFailure(Call<Sales> call, Throwable t) {
        t.printStackTrace();
        eventBus.post(new SalesFetched(false));
    }
}
