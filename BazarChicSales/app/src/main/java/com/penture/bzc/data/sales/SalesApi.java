package com.penture.bzc.data.sales;

import com.penture.bzc.domain.entities.Sales;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by kevin on 07/03/2017.
 */

public interface SalesApi {

    @GET("sales/active.json")
    Call<Sales> getActiveSales();
}
