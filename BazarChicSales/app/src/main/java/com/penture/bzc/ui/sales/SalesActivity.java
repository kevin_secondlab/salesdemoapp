package com.penture.bzc.ui.sales;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.View;

import butterknife.BindView;

import com.penture.bzc.R;
import com.penture.bzc.domain.entities.Sale;
import com.penture.bzc.domain.sales.SalesManager;
import com.penture.bzc.domain.sales.events.SalesFetched;
import com.penture.bzc.ui.common.BaseActivity;
import com.penture.bzc.ui.saleDetail.SaleDetailFragment;
import com.squareup.otto.Subscribe;

import java.util.List;

/**
 * Created by kevin on 07/03/2017.
 */

public class SalesActivity extends BaseActivity implements SalesFragment.SalesContainer {

    @Nullable
    @BindView(R.id.sale_detail)
    View saleDetail;

    private SalesFragment salesFragment;
    private SaleDetailFragment saleDetailFragment;
    private static boolean isFirstLaunch = true;

    protected @LayoutRes int getLayoutId() {
        return R.layout.activity_sales;
    }

    protected boolean isDualPanel() {
        return saleDetail != null;
    }

    protected void onFirstLaunch() {
        SalesManager.getInstance().fetchActiveSales();
    }

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        salesFragment = (SalesFragment) getFragmentManager().findFragmentById(R.id.sales);
        saleDetailFragment =
                (SaleDetailFragment) getFragmentManager().findFragmentById(R.id.sale_detail);
    }

    @Override protected void onStart() {
        super.onStart();

        List<Sale> sales = SalesManager.getInstance().getActiveSales();

        if (isFirstLaunch) {
            isFirstLaunch = false;
            onFirstLaunch();
        } else {
            if (sales.size() > 0) {
                salesFragment.updateSalesList(sales);
                if (isDualPanel()) showSaleDetail(sales.get(0), 0);
            }
        }
    }

    @Override
    public void showSaleDetail(Sale sale, int position) {
            if (isDualPanel() && saleDetailFragment != null) {
                saleDetailFragment.displaySale(sale, true);
            } else {
                startSaleDetailActivity(position);
            }
    }

    @Subscribe
    public void doThis(SalesFetched salesFetched) {
        if (!salesFetched.isSuccess()) return;

        List<Sale> sales = SalesManager.getInstance().getActiveSales();
        if (salesFragment != null) salesFragment.updateSalesList(sales);
        if (isDualPanel() && sales.size() > 0) showSaleDetail(sales.get(0), 0);
    }
}
