package com.penture.bzc.domain.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kevin on 07/03/2017.
 */

public class Image {
    @SerializedName("url") private String url;
    @SerializedName("width") private String width;
    @SerializedName("height") private String height;

    public String getUrl() {
        return url;
    }

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }
}
