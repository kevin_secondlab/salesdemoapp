package com.penture.bzc.ui.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.penture.bzc.R;
import com.penture.bzc.common.eventBus.Messenger;
import com.penture.bzc.ui.saleDetail.SaleDetailActivity;
import com.penture.bzc.ui.sales.SalesActivity;

/**
 * Created by kevin on 07/03/2017.
 */
public abstract class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    @Nullable
    Toolbar toolbar;

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(R.string.app_name);
        }
    }

    @Override protected void onResume() {
        super.onResume();
        Messenger.getInstance().register(this);
    }

    @Override protected void onPause() {
        super.onPause();
        Messenger.getInstance().unregister(this);
    }

    abstract protected int getLayoutId();

    protected void startSalesActivity() {
        Intent intent = new Intent(this, SalesActivity.class);
        startActivity(intent);
        finish();
    }

    protected void startSaleDetailActivity(int position) {
        Intent intent = new Intent(this, SaleDetailActivity.class);
        intent.putExtra("position", position);
        startActivity(intent);
    }
}
